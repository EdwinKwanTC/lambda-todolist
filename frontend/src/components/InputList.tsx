import React, { useState } from "react";

import {
  FormControl,
  FormLabel,
  FormHelperText,
  Input,
  Grid,
  Button,
} from "@chakra-ui/react";

// axios
import axios from "axios";

export const InputList = () => {
  const [inputItem, setInputItem] = useState<string>("");

  const postItem = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    console.log("submit");
    if (!inputItem) {
      return;
    }

    await axios
      .post(
        "https://31hjxelnr9.execute-api.ap-southeast-1.amazonaws.com/test",
        {
          item: inputItem,
        }
      )
      .then(function (response) {
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      });

    setInputItem("");
  };

  return (
    <Grid margin="5%">
      <form onSubmit={postItem}>
        <FormControl>
          <FormLabel htmlFor="item">Please enter todo list item:</FormLabel>
          <Input
            id="item"
            type="text"
            value={inputItem}
            onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
              setInputItem(event.target.value)
            }
          />
          <FormHelperText>All item will be shown below.</FormHelperText>
          <Button colorScheme="blue" type="submit" width="100px" margin="20px">
            Submit
          </Button>
        </FormControl>
      </form>
    </Grid>
  );
};

export default InputList;
